use std::collections::HashMap;
use reqwest;
use serde::{Serialize};
use std::marker::Sized;
use std::env;
use std::convert::AsRef;


enum Path {
    New,
    Register,
    State,
    Delete
}

impl AsRef<str> for Path {
    fn as_ref(&self) -> &str {
        match *self {
            Path::New => "new",
            Path::Register => "register",
            Path::State => "state",
            Path::Delete => "delete",
        }
    }
}


#[derive(Serialize, Deserialize, Debug)]
struct Data {
    key: String,
    state: State,
}
#[derive(Serialize, Deserialize, Debug)]
struct ApiResult {
    status: String,
    message: Option<String>,
    data: Option<Data>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct State {
    active: bool,
    board: Option<[[Option<String>; 7]; 6]>,
    clients: Vec<String>,
    client_colours: HashMap<String, String>,
    current_turn: Option<String>,
    turn_number: Option<String>,
    winner: Option<String>,
    completion_state: String,
    game_type: String,
}


fn build_url(path: Path) -> Result<reqwest::Url, reqwest::UrlError> {
    let url = &env::var("API_ENDPOINT_URL")
        .expect("Misising env var API_ENDPOINT_URL")[..];
    let url = reqwest::Url::parse(url)?
        .join("connect-four/")?
        .join(path.as_ref())?;
    Ok(url)
}

fn api_request(
    client: &reqwest::Client, path: Path, method: reqwest::Method, key: &str
) -> Result<reqwest::Response, reqwest::Error> {
    let empty_data: HashMap<&str, &str> = HashMap::new();
    api_request_with_data(&client, path, method, &key, &empty_data)
}

fn api_request_with_data<T: Serialize + ?Sized>(
    client: &reqwest::Client, path: Path, method: reqwest::Method, key: &str, data: &T
) -> Result<reqwest::Response, reqwest::Error> {
    header! { (XApiKey, "x-api-key") => [String] }
    let url = build_url(path).expect("Unable to build url");
    let api_key = env::var("API_KEY")
        .expect("Missing env var API_KEY");
    let response = client.request(method, url)
        .header(XApiKey(api_key))
        .json(data)
        .query(&[("key", key)])
        .send()?;
    Ok(response)
}

pub fn make_request() -> Result<reqwest::Response, reqwest::Error> {
    let client = reqwest::Client::new();
    start_new_game(&client, "some_key")
}


pub fn start_new_game(client: &reqwest::Client, key: &str)
                      -> Result<reqwest::Response, reqwest::Error> {
    api_request(&client, Path::New, reqwest::Method::Post, key)
}

pub fn register_for_game(client: &reqwest::Client, key: &str, player_name: &str)
                         -> Result<reqwest::Response, reqwest::Error> {
    let mut data:HashMap<&str, &str> = HashMap::new();
    data.insert("clientName", player_name);
    api_request_with_data(
        &client, Path::Register, reqwest::Method::Post, key, &data
    )
}

pub fn get_state(client: &reqwest::Client, key: &str)
                 -> Result<State, reqwest::Error> {
    let mut response = api_request(
        &client, Path::State, reqwest::Method::Get, key
    )?;
    let result:ApiResult = response.json()?;
    Ok(result.data.unwrap().state)
}

pub fn delete_game(client: &reqwest::Client, key: &str)
                   -> Result<reqwest::Response, reqwest::Error> {
    api_request(&client, Path::Delete, reqwest::Method::Post, key)
}

pub fn post_changed_state(client: &reqwest::Client, key: &str, state: State)
                          -> Result<reqwest::Response, reqwest::Error> {
    let d = Data { key: key.to_string(), state: state };
    api_request_with_data(&client, Path::State, reqwest::Method::Post, key, &d)
}


#[cfg(test)]
mod tests {
    use super::*;
    use hyper::{StatusCode};


    static TEST_KEY:&str = "test_key";  // global just for tests


    fn clear_test_game_for_new_test(client: &reqwest::Client) {
        delete_game(&client, TEST_KEY).unwrap();  // we don't care if we get a bad request error
    }

    fn set_up_test() -> reqwest::Client {
        let client = reqwest::Client::new();
        clear_test_game_for_new_test(&client);  // remove previous game
        client  // if we want to set up more than just a client we can just use a struct
    }

    #[test]
    fn test_new_game() {
        let client = set_up_test();

        let mut response = start_new_game(&client, TEST_KEY).unwrap();
        assert_eq!(response.status(), StatusCode::Ok);

        let result:ApiResult = response.json().unwrap();
        println!("result: {:?}", result);
        let state = result.data.unwrap().state;
        assert!(!state.active);
        assert_eq!(state.board, None);
        assert_eq!(state.current_turn, None);
        assert_eq!(state.winner, None);
    }

    #[test]
    fn test_delete_game() {
        let client = set_up_test();
        start_new_game(&client, TEST_KEY).unwrap();
        let response = delete_game(&client, TEST_KEY).unwrap();
        assert_eq!(response.status(), StatusCode::Ok);
        let second_response = delete_game(&client, TEST_KEY).unwrap();
        assert_eq!(second_response.status(), StatusCode::BadRequest);
    }

    #[test]
    fn test_register_player() {

    }
}
