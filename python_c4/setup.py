from setuptools import setup, find_packages

requires = [
    'requests',
    'pandas',
]
setup(
    name='connect-four',
    version="1.0.0",
    description='connect four clients',
    classifiers=[
        "Programming Language :: Python",
    ],
    author='Tony Cox',
    author_email='coxy.t82@gmail.com',
    url='',
    keywords='',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    # tests_require=test_deps,
    # extras_require=extras,
)
