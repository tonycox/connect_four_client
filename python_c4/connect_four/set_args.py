import json
import os
import sys


if __name__ == '__main__':
    args = input("Enter args separated by spaces: ")
    args = args.split()
    os.environ['ARGS'] = json.dumps(args)
    sys.exit(0)