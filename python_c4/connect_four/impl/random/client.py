import random
import logging

from connect_four.client.client import Client

logger = logging.getLogger(__name__)


class RandomClient(Client):

    def move(self):
        legal_moves = []
        for col_idx, col in enumerate(self.board_df.columns):
            for row_idx, cell in self.board_df[col].iteritems():
                if cell is None:
                    legal_moves.append((row_idx, col_idx))
                    break  # only breaks outer loop - this is the legal move for this column, we can't keep looking
        random_move = random.choice(legal_moves)
        self.board_df.loc[random_move] = self.colour
        logger.info("Move complete: Col {}, Row {}".format(random_move[1], random_move[0]))