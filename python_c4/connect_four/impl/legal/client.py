import logging

from connect_four.client.client import Client


logger = logging.getLogger(__name__)


class LegalClient(Client):

    def move(self):
        for row_idx, row in self.board_df.iterrows():
            for col_idx, cell in enumerate(row):
                if cell is None:
                    self.board_df.loc[row_idx, col_idx] = self.colour
                    logger.info("Move complete: Col {}, Row {}".format(col_idx, row_idx))
                    return
