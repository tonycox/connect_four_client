import os
import json
from urllib.parse import urljoin
import logging

import requests


logger = logging.getLogger(__name__)

API_ENDPOINT_URL = os.getenv('API_ENDPOINT_URL')
API_KEY = os.getenv('API_KEY')
GET = 'GET'
POST = 'POST'


def api_request(path: str, method: str, params: dict=None, data: dict=None, **request_kwargs) -> dict:
    assert not (method == GET and data is not None), "Cannot provide data with GET request"
    response = requests.request(
        method=method,
        url=urljoin(urljoin(API_ENDPOINT_URL, 'connect-four/'), path),
        headers={
            'Content-type': 'application/json',
            'x-api-key': API_KEY
        },
        params=params,
        data=None if data is None else json.dumps(data),
        **request_kwargs
    )
    if response.status_code != 200:
        logger.error(response.text)
    response_data = response.json()
    return response_data


def start_new_game(key:str =None):
    return api_request(
        path='new',
        method=POST,
        params=None if key is None else {'key': key},
    )


def register_client(key: str, client_name: str):
    return api_request(
        path='register',
        method=POST,
        params={'key': key},
        data={'clientName': client_name}
    )


def get_state(key: str):
    return api_request(
        path='state',
        method=GET,
        params={'key': key}
    )['data']['state']


def set_state(key: str, state: dict):
    return api_request(
        path='state',
        method=POST,
        params={'key': key},
        data={'state': state}
    )