import logging
import argparse
import sys
import os
import json

from connect_four import api_interface
from connect_four import impl
logger = logging.getLogger(__name__)

CLIENTS = {
    'random': impl.RandomClient,
    'legal': impl.LegalClient
}

def setup_logging():
    logging.basicConfig(format='%(asctime)s | %(levelname)s\t| %(name)s |  %(message)s', level=logging.DEBUG)


def _get_parser():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='command')
    new_parser = subparsers.add_parser('new', help='start a new game')
    new_parser.add_argument('--key', dest='key', type=str, help='Unique game key. One will be assigned if not provided')
    new_parser.set_defaults(func=new)
    register_parser = subparsers.add_parser('register', help='register a client')
    register_parser.add_argument('key', type=str, help='Game to register for')
    register_parser.add_argument('client', type=str, help='Name of client to register')
    register_parser.set_defaults(func=register)
    play_parser = subparsers.add_parser('play', help='play game')
    play_parser.add_argument('key', type=str, help='Game to play for')
    play_parser.add_argument('client', type=str, help='Name of registered game client')
    play_parser.add_argument('player', type=str, help='[{}]'.format(','.join(CLIENTS.keys())))
    play_parser.set_defaults(func=play)
    play_parser = subparsers.add_parser('activate', help='start game')
    play_parser.add_argument('key', type=str, help='Game to set active')
    play_parser.set_defaults(func=activate)
    return parser

def parse_args(args=None):
    parser = _get_parser()
    arg_dict = vars(parser.parse_args(args))
    func = arg_dict.pop('func', None)
    if func is None:
        parser.print_usage(sys.stderr)
        sys.stderr.write('run.py error: a subcommand {new,register,...} must be specified\n')
        sys.exit(2)
    return func, arg_dict


def new(key):
    data = api_interface.start_new_game(key=key).get('data')
    logger.info('New game started with key {}'.format(data.get('key')))


def register(key, client):
    data = api_interface.register_client(key=key, client_name=client).get('data')
    logger.info('Client {} successfully registered for game {}'.format(client, data['key']))


def activate(key):
    state = api_interface.get_state(key)
    state['active'] = True
    api_interface.set_state(key, state)
    logger.info('Game {} successfully activated'.format(key))


def play(key, client, player):
    assert player in CLIENTS.keys(), 'Unknown player: {}'.format(player)
    state = api_interface.get_state(key)
    assert client in state['clients'], 'Client {} is not registered for game {}'.format(client, key)
    game_client = CLIENTS[player](key, client)
    game_client.play_game()
    logger.info('Game Over')
    state = api_interface.get_state(key)
    logger.info('Final State: {}'.format(state))


if __name__ == '__main__':
    setup_logging()
    _, argv = argparse.ArgumentParser().parse_known_args()
    if len(argv) == 0:
        _get_parser().print_usage()
        args = input("Enter args separated by spaces: ")
        argv = args.split()
    func, arg_dict = parse_args(argv)
    func(**arg_dict)