import logging
import time
import pandas as pd

from connect_four import api_interface as api

logger = logging.getLogger(__name__)


class Client(object):

    def __init__(self, game_key, name) -> None:
        self.game_key = game_key
        self.name = name
        self.game_state = None
        self.board_df = None
        super().__init__()

    def _game_is_active(self):
        return self.game_state['active'] is True

    def _is_my_turn(self):
        return self.game_state['current_turn'] == self.name

    def _update_game_state(self):
        self.game_state = api.get_state(self.game_key)

    def _update_board(self):
        self.board_df = pd.DataFrame(self.game_state.get('board'))

    def _send_move_to_api(self):
        self.game_state['board'] = self.board_df.values.tolist()
        api.set_state(self.game_key, self.game_state)

    @property
    def colour(self):
        if self.game_state is None:
            return None
        return self.game_state['client_colours'][self.name]

    def play_game(self):
        while True:
            self._update_game_state()
            if not self._game_is_active():
                # todo: check who won or if stalemate or if not started yet
                logger.info('Game is not active. Stopping.')
                break
            if self._is_my_turn():
                self._update_board()
                self.move()
                self._send_move_to_api()
            time.sleep(1)

    def move(self):
        """Modifies the board dataframe with the new move
        Override this method to implement move logic.  This should be the only thing that needs to be
        overridden for each player

        :return: None
        """
        raise NotImplementedError
